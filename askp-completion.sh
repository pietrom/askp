#!/bin/bash

_askp_completion() {
  latest="${COMP_WORDS[$COMP_CWORD]}"
  prev="${COMP_WORDS[$COMP_CWORD - 1]}"
  words=""
  case "${prev}" in
    askp)
      words="create add"
      ;;
    create)
      words="kotlin-single kotlin-multi"
      ;;
    add)
      words="kotlin-lib kotlin-micronaut kotlin-quarkus kotlin-spring-boot java-lib java-micronaut java-quarkus java-springboot"
      ;;
    *)
      ;;
  esac
  COMPREPLY=($(compgen -W "$words" -- $latest))
  return 0
}

complete -D askp
complete -F _askp_completion askp

#!/bin/bash

for t in $(find test -name '*.sh') ; do
	. "$t"
done

test_cases=$(declare -F | sed 's/declare -f //' | grep '^test_')

total=0
failures=0
failed_test_cases=()
for tc in $test_cases ; do
	echo "Executing ${tc}..."
	$tc
	if [ $? -ne 0 ] ; then
		failures=$(expr ${failures} + 1)
		failed_test_cases+=(${tc})
	fi
	total=$(expr ${total} + 1)
done

echo "Test suite completed: total test cases ran: ${total}. Failures: ${failures}"

if [ $failures -ne 0 ] ; then
	echo "Failed test cases:"
	for ftc in "${failed_test_cases[@]}" ; do
		echo " ${ftc}"
	done
	exit 1
fi
exit 0